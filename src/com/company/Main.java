package com.company;

import java.util.Scanner;

public class Main {

    public static int terminal(int[] denominationOfMoney, int index, int change){
        int quantity = change / denominationOfMoney[index];
        String rub = denominationOfMoney[index] + " рублей:" + quantity + " шт.";
        System.out.print(rub);
        change = change - (denominationOfMoney[index] * quantity);
        return change;
    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Стоимость товара:");
        int productPrice = scanner.nextInt();
        System.out.println("Укажите внесённую сумму:");
        int amountOfMoney = scanner.nextInt();
        int[] denominationOfMoney = {1, 2, 5, 10, 50, 100, 200, 500, 1000, 2000, 5000};
        int index;
        int change = amountOfMoney - productPrice;
        System.out.println("Ваша сдача:" + change);
        System.out.print("Терминал выдаёт: ");
        while (change != 0) {
           out: if (change >= 5000) {
                index = 10;
                change = terminal(denominationOfMoney, index, change);
            } else if (change >= 2000) {
                index = 9;
               change = terminal(denominationOfMoney, index, change);
            } else if (change >= 1000) {
                index = 8;
               change = terminal(denominationOfMoney, index, change);
            } else if (change >= 500) {
                index = 7;
               change = terminal(denominationOfMoney, index, change);
            } else if (change >= 200) {
                index = 6;
               change = terminal(denominationOfMoney, index, change);
            } else if (change >= 100) {
                index = 5;
               change = terminal(denominationOfMoney, index, change);
            } else if (change >= 50) {
                index = 4;
               change = terminal(denominationOfMoney, index, change);
            } else if (change >= 10) {
                index = 3;
               change = terminal(denominationOfMoney, index, change);
            } else if (change >= 5) {
                index = 2;
               change = terminal(denominationOfMoney, index, change);
            } else if (change >= 2) {
                index = 1;
               change = terminal(denominationOfMoney, index, change);
            } else {
                index = 0;
               change = terminal(denominationOfMoney, index, change);
            }
            System.out.print("; ");
        }
    }
}
